"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gClassId = 0;
let gStudentId = 0;
let gSelectClass = $('#select-class')

let studentTable = $('#student-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'code' },
        { data: 'name' },
        { data: 'gender' },
        { data: 'dateOfBirth', render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' )  },
        { data: 'address' },
        { data: 'phone' },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let _class = {
    newClass: {
        code: '',
        name: '',
        teacherName: '',
        teacherPhone: ''
    },
    // CREATE - Tạo lớp mới
    onCreateNewClassClick() {
        $('#modal-create-class').modal('show');
        console.log(gClassId);
    },
    onUpdateClassClick() {
        if (gClassId != 0) {
            $('#modal-create-class').modal('show');
            $.get(`http://localhost:8080/class/details/${gClassId}`, loadClassToInput);
        } else {
            alert('Please select a class to update');
        }
    },
    onSaveClassClick() {
        this.newClass = {
            code: $('#inp-class-code').val().trim(),
            name: $('#inp-class-name').val().trim(),
            teacherName: $('#inp-teacher-name').val().trim(),
            teacherPhone: $('#inp-teacher-phone').val().trim(),
        };
        if (gClassId == 0) {
            if (validateClass(this.newClass)) {
                $.ajax({
                    url: `http://localhost:8080/class/create`,
                    method: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(this.newClass),
                    success: () => {
                        alert('successfully create new Class');
                        location.reload();
                    },
                    error: (err) => alert(err.responseText),
                });
            }
        } else {
            if (validateClass(this.newClass)) {
                $.ajax({
                    url: `http://localhost:8080/class/update/${gClassId}`,
                    method: 'PUT',
                    contentType: 'application/json',
                    data: JSON.stringify(this.newClass),
                    success: () => {
                        alert('successfully update Class with id: ' + gClassId);
                        location.reload();
                    },
                    error: (err) => alert(err.responseText),
                });
            }
        }
    },
    onDeleteClassClick() {
        if (gClassId != 0) {
            $('#modal-delete-class').modal('show');
        } else {
            alert('Please select a class to delete');
        }
    },
    onConfirmDeleteClassClick() {
        $.ajax({
            url: `http://localhost:8080/class/delete/${gClassId}`,
            method: 'delete',
            success: () => {
                alert('successfully delete class with id: ' + gClassId);
                location.reload();
            },
            error: (err) => alert(err.responseText),
        });
    },
};

let student = {
    newStudent: {
        code: "",
        name: "",
        gender: "",
        dateOfBirth: "",
        address: "",
        phone: ""
    },
    onNewStudentClick() {
        gStudentId = 0;
        this.newStudent = {
            code: $('#inp-student-code').val().trim(),
            name: $('#inp-student-name').val().trim(),
            gender: $('#select-gender option:selected').val(),
            dateOfBirth: $('#inp-student-dob').val().trim(),
            address: $('#text-student-address').val().trim(),
            phone: $('#inp-student-phone').val().trim()
        }
        if (validateStudent(this.newStudent)) {
            if (gClassId == 0) {
                alert(`Please select class to create a new student`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/student/create/${gClassId}/`,
                    method: 'POST',
                    data: JSON.stringify(this.newStudent),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Student created successfully`);
                        $.get(
                            `http://localhost:8080/class/${gClassId}/students`,
                            loadStudentToTable,
                        );
                        resetStudentInput();
                    },
                });
            }
        }
    },
    onEditStudentClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = studentTable.row(vSelectedRow).data();
        gStudentId = vSelectedData.id;
        $.get(
            `http://localhost:8080/student/details/${gStudentId}`,
            loadStudentToInput
        );
    },
    onUpdateStudentClick() {
        this.newStudent = {
            code: $('#inp-student-code').val().trim(),
            name: $('#inp-student-name').val().trim(),
            gender: $('#select-gender option:selected').val(),
            dateOfBirth: $('#inp-student-dob').val().trim(),
            address: $('#text-student-address').val().trim(),
            phone: $('#inp-student-phone').val().trim()
        };
        if (validateStudent(this.newStudent)) {
            if (gClassId == 0) {
                alert(`Please select class to update a new student`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/student/update/${gStudentId}/`,
                    method: 'PUT',
                    data: JSON.stringify(this.newStudent),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Student updated successfully`);
                        $.get(
                            `http://localhost:8080/class/${gClassId}/students`,
                            loadStudentToTable,
                        );
                        resetStudentInput();
                    },
                });
            }
        }
    },
    onDeleteStudentByIdClick() {
        $('#modal-delete-student').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = studentTable.row(vSelectedRow).data();
        gStudentId = vSelectedData.id;
    },
    oConfirmDeleteStudentClick() {
        $.ajax({
            url: `http://localhost:8080/student/delete/${gStudentId}`,
            method: 'delete',
            success: () => {
                alert(`Student with id ${gStudentId} was successfully deleted`);
                $.get(`http://localhost:8080/student/all`, loadStudentToTable);
                $('#modal-delete-student').modal('hide');
            },
            error: (err) => alert(err.responseText),
        });
    },
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/class/all`, loadClassToSelect);
$.get(`http://localhost:8080/student/all`, loadStudentToTable);

gSelectClass.change(onGetStudentsChange);

$('#btn-create-class').click(_class.onCreateNewClassClick);
$('#btn-update-class').click(_class.onUpdateClassClick);
$('#btn-save-class').click(_class.onSaveClassClick);
$('#btn-delete-class').click(_class.onDeleteClassClick);
$('#btn-confirm-delete-class').click(_class.onConfirmDeleteClassClick);

$('#btn-create-student').click(student.onNewStudentClick);
$('#btn-update-student').click(student.onUpdateStudentClick);
$('#btn-confirm-delete-student').click(student.oConfirmDeleteStudentClick);

$('#student-table').on('click', '.fa-edit', student.onEditStudentClick);
$('#student-table').on('click', '.fa-trash', student.onDeleteStudentByIdClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Chọn lớp thì load học sinh ra bảng
function onGetStudentsChange(event) {
    gClassId = event.target.value;
    if (gClassId == 0) {
        $.get(`http://localhost:8080/student/all`, loadStudentToTable);
    } else {
        $.get(
            `http://localhost:8080/class/${gClassId}/students`,
            loadStudentToTable,
        );
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function loadClassToSelect(paramClass) {
    paramClass.forEach((Class) => {
        $('<option>', {
            text: Class.name,
            value: Class.id,
        }).appendTo(gSelectClass);
    });
}
function loadStudentToTable(paramStudent) {
    studentTable.clear();
    studentTable.rows.add(paramStudent);
    studentTable.draw();
}
function loadClassToInput(paramClass) {
    $('#inp-class-code').val(paramClass.code);
    $('#inp-class-name').val(paramClass.name);
    $('#inp-teacher-name').val(paramClass.teacherName);
    $('#inp-teacher-phone').val(paramClass.teacherPhone);
}
function validateClass(paramClass) {
    let vResult = true;
    try {
        if (paramClass.code == '') {
            vResult = false;
            throw `code can't be empty`;
        }
        if (paramClass.code.length < 3) {
            vResult = false;
            throw `Mã lớp phải có ít nhất 3 ký tự`;
        }

        if (paramClass.name == '') {
            vResult = false;
            throw ` name can't be empty`;
        }
        if (paramClass.name.length < 3) {
            vResult = false;
            throw `Tên lớp phải có ít nhất 3 ký tự`;
        }

        if (paramClass.teacherPhone == '') {
            vResult = false;
            throw `phone can't be empty`;
        }
        if (paramClass.teacherPhone.length < 10 || isNaN(paramClass.teacherPhone)) {
            vResult = false;
            throw `Cần nhập đúng kiểu số điện thoại phải không có chữ cái và đúng 10 số`;
        }

        if (paramClass.teacherName == '') {
            vResult = false;
            throw `teacher's name can't be empty`;
        }
        if (paramClass.teacherName.length < 3) {
            vResult = false;
            throw `Tên giáo viên phải có ít nhất 3 ký tự`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function validateStudent(paramStudent) {
    let vResult = true;
    try {
        if (paramStudent.code == '') {
            vResult = false;
            throw `Student code can't empty`;
        }
        if (paramStudent.code.length < 3) {
            vResult = false;
            throw `Mã học sinh phải có ít nhất 3 ký tự`;
        }

        if (paramStudent.name == '') {
            vResult = false;
            throw `Student name can't empty`;
        }
        if (paramStudent.name.length < 3) {
            vResult = false;
            throw `Tên học sinh phải có ít nhất 3 ký tự`;
        }

        if (paramStudent.gender == '') {
            vResult = false;
            throw `Must choose a gender`;
        }
        if (paramStudent.dateOfBirth == '') {
            vResult = false;
            throw `Date of birth can't empty`;
        }

        if (paramStudent.address == '') {
            vResult = false;
            throw `Address can't empty`;
        }
        if (paramStudent.address.length < 3) {
            vResult = false;
            throw `Địa chỉ học sinh phải có ít nhất 3 ký tự`;
        }

        if (paramStudent.phone == '') {
            vResult = false;
            throw `Phone can't empty`;
        }
        if (paramStudent.phone.length < 10 || isNaN(paramStudent.phone)) {
            vResult = false;
            throw `Cần nhập đúng kiểu số điện thoại phải không có chữ cái và đúng 10 số`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function resetStudentInput() {
    $('#inp-student-code').val('');
    $('#inp-student-name').val('');
    $('#select-gender').val('');
    $('#inp-student-dob').val('');
    $('#text-student-address').val('');
    $('#inp-student-phone').val('');
}
function loadStudentToInput(paramStudent) {
    $('#inp-student-code').val(paramStudent.code);
    $('#inp-student-name').val(paramStudent.name);
    $('#select-gender').val(paramStudent.gender);
    $('#inp-student-dob').val(paramStudent.dateOfBirth);
    $('#text-student-address').val(paramStudent.address);
    $('#inp-student-phone').val(paramStudent.phone);
}

