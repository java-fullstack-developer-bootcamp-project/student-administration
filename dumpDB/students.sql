-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 14, 2022 at 12:06 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `midterm_test_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `lop_hoc_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `address`, `code`, `date_of_birth`, `gender`, `name`, `phone`, `lop_hoc_id`) VALUES
(1, '01 Nguyễn Trãi', 'NgaN1', '2006-09-03', 'FEMALE', 'Nguyễn Nga', '0989213122', 2),
(2, '03 Lê Duẫn', 'TriT1', '2006-11-12', 'MALE', 'Trần Trí', '0989674567', 2),
(3, '10 Trần Hưng Đạo', 'NhiH1', '2006-12-01', 'FEMALE', 'Huỳnh Nhi', '0967237856', 2),
(4, '07 Nguyễn Thiện Thuật', 'TinN1', '2006-02-05', 'MALE', 'Nguyễn Tín', '0967457845', 1),
(5, '15 An Bình', 'KietH1', '2006-09-01', 'MALE', 'Huỳnh Kiệt', '0978638967', 1),
(6, '100 Bùi Hữu Nghĩa', 'VanL1', '2006-08-19', 'FEMALE', 'Lê Vân', '0998682367', 1),
(7, '25 Lê Lợi', 'NghiaN1', '2006-04-05', 'MALE', 'Nguyễn Nghĩa', '0912783489', 3),
(8, '80 Nguyễn Công Trứ', 'NganN1', '2006-07-20', 'FEMALE', 'Nguyễn Ngân', '0989236767', 3),
(9, '65 Phạm Ngũ Lão', 'DuongL1', '2006-06-15', 'MALE', 'Lâm Dương', '0989892368', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY IF NOT EXISTS (`id`),
  ADD UNIQUE KEY IF NOT EXISTS `UK_eqa1d4jiyg5m5rnuja7ifgw73` (`code`),
  ADD UNIQUE KEY IF NOT EXISTS `UK_4j48kma5fa3dcya13gd0l3gi` (`phone`),
  ADD KEY IF NOT EXISTS`FK1im9ht168r5hrr02eycs0j5fe` (`lop_hoc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `FK1im9ht168r5hrr02eycs0j5fe` FOREIGN KEY (`lop_hoc_id`) REFERENCES `classes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
