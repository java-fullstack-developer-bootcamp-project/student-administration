package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CClass;

public interface iClassRepository extends JpaRepository<CClass, Long> {
    
}
