package com.devcamp.restapi.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "classes")
public class CClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập mã lớp")
    @Size(min = 3, message = "Mã lớp phải có ít nhất 3 ký tự ")
    @Column(name = "code", unique = true)
    private String code;

    @NotNull(message = "Nhập tên lớp")
    @Size(min = 3, message = "Tên lớp phải có ít nhất 3 ký tự ")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Nhập tên giáo viên")
    @Size(min = 3, message = "Tên giáo viên phải có ít nhất 3 ký tự ")
    @Column(name = "teacher_name")
    private String teacherName;

    @NotNull(message = "Nhập số điện thoại giáo viên")
    @Size(max = 10, message = "Số điện thoại có độ dài 10 ký tự ")
    @Column(name = "teacher_phone")
    private String teacherPhone;

    @OneToMany(targetEntity = CStudent.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "lop_hoc_id", insertable=false, updatable=false)
    private List<CStudent> students;

    public CClass() {
    }

    public CClass(long id, String code, String name, String teacherName, String teacherPhone, List<CStudent> students) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.teacherName = teacherName;
        this.teacherPhone = teacherPhone;
        this.students = students;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public List<CStudent> getStudents() {
        return students;
    }

    public void setStudents(List<CStudent> students) {
        this.students = students;
    }

    
    
}
