package com.devcamp.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CStudent;

public interface iStudentRepository extends JpaRepository<CStudent, Long> {

    List<CStudent> findByLopHocId(Long classId);
    
}
