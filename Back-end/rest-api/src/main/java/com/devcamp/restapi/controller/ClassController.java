package com.devcamp.restapi.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.restapi.model.CClass;
import com.devcamp.restapi.repository.iClassRepository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ClassController {
    @Autowired
    private iClassRepository classRepository;

    // 1. CREATE - Create a new class
    @CrossOrigin
	@PostMapping("/class/create")
	public ResponseEntity<Object> createClass(@RequestBody CClass cClass) {
        try {
            CClass newClass = new CClass();
            newClass.setCode(cClass.getCode());
            newClass.setName(cClass.getName());
            newClass.setTeacherName(cClass.getTeacherName());
            newClass.setTeacherPhone(cClass.getTeacherPhone());
            newClass.setStudents(cClass.getStudents());
            CClass savedClass = classRepository.save(newClass);
            return new ResponseEntity<>(savedClass, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Class: "
			+e.getCause().getCause().getMessage());
        }
    }

    // 2. UPDATE - Update an existing class by class id
    @CrossOrigin
	@PutMapping("/class/update/{id}")
    public ResponseEntity<Object> updateClass(@PathVariable("id") Long id, @RequestBody CClass cClass) {
        try {
            Optional<CClass> classData = classRepository.findById(id);
            if (classData.isPresent()) {
                CClass updatedClass = classData.get();
                updatedClass.setCode(cClass.getCode());
                updatedClass.setName(cClass.getName());
                updatedClass.setTeacherName(cClass.getTeacherName());
                updatedClass.setTeacherPhone(cClass.getTeacherPhone());
                updatedClass.setStudents(cClass.getStudents());
                CClass savedClass = classRepository.save(updatedClass);
                return new ResponseEntity<>(savedClass, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 3.1 READ - Get all classes
    @CrossOrigin
	@GetMapping("/class/all")
    public List<CClass> getAllClasses() {
        return classRepository.findAll();
    }

    // 3.2 READ - Get a specified class by id
    @CrossOrigin
	@GetMapping("/class/details/{id}")
	public CClass getClassById(@PathVariable Long id) {
		if (classRepository.findById(id).isPresent())
			return classRepository.findById(id).get();
		else
			return null;
	}

    // 4. DELETE - Delete a specified class by id
    @CrossOrigin
	@DeleteMapping("/class/delete/{id}")
	public ResponseEntity<Object> deleteClassById(@PathVariable Long id) {
		try {
			classRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
