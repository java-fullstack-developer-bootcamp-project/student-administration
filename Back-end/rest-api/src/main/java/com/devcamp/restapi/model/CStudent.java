package com.devcamp.restapi.model;

import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "students")
public class CStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập mã học sinh")
    @Size(min = 3, message = "Mã học sinh phải có ít nhất 3 ký tự ")
    @Column(name = "code", unique = true)
    private String code;

    @NotNull(message = "Nhập tên học sinh")
    @Size(min = 3, message = "Tên học sinh phải có ít nhất 3 ký tự ")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Nhập giới tính học sinh")
    @Column(name = "gender", columnDefinition = "ENUM('MALE', 'FEMALE')")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull(message = "Nhập ngày tháng năm sinh học sinh")
    @Column(name = "date_of_birth")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    @NotNull(message = "Nhập địa chỉ học sinh")
    @Size(min = 3, message = "Địa chỉ học sinh phải có ít nhất 3 ký tự ")
    @Column(name = "address")
    private String address;

    @NotNull(message = "Nhập số điện thoại học sinh")
    @Size(max = 10, message = "Số điện thoại có độ dài 10 ký tự ")
    @Column(name = "phone", unique = true, updatable = true)
    private String phone;

    @ManyToOne
    @JsonIgnore
    private CClass lopHoc;

    public CStudent() {
    }

    public CStudent(long id, String code, String name, Gender gender, LocalDate dateOfBirth, String address,
            String phone, CClass lopHoc) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phone = phone;
        this.lopHoc = lopHoc;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CClass getLopHoc() {
        return lopHoc;
    }

    public void setLopHoc(CClass lopHoc) {
        this.lopHoc = lopHoc;
    }

    

   
   
}
