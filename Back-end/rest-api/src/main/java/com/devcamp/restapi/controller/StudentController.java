package com.devcamp.restapi.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.restapi.model.*;
import com.devcamp.restapi.repository.iClassRepository;
import com.devcamp.restapi.repository.iStudentRepository;


@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class StudentController {
    @Autowired
    private iStudentRepository studentRepository;

    @Autowired
    private iClassRepository classRepository;

    // 1. CREATE - Create a new student
    @CrossOrigin
    @PostMapping("/student/create/{id}")
    public ResponseEntity<Object> createStudent(@PathVariable("id") Long id, @RequestBody CStudent cStudent) {
        try {
            Optional<CClass> classData = classRepository.findById(id);
            if (classData.isPresent()) {
                CStudent newStudent = new CStudent();
                newStudent.setCode(cStudent.getCode());
                newStudent.setName(cStudent.getName());
                newStudent.setGender(cStudent.getGender());
                newStudent.setPhone(cStudent.getPhone());
                newStudent.setDateOfBirth(cStudent.getDateOfBirth());
                newStudent.setAddress(cStudent.getAddress());

                CClass _class = classData.get();
                newStudent.setLopHoc(_class);

                CStudent savedStudent = studentRepository.save(newStudent);
                return new ResponseEntity<>(savedStudent, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Student: "
                    + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // 2. UPDATE - Update an existing student by id
    @CrossOrigin
    @PutMapping("/student/update/{id}")
    public ResponseEntity<Object> updateStudent(@PathVariable("id") Long id, @RequestBody CStudent cStudent) {
        try {
            Optional<CStudent> studentData = studentRepository.findById(id);
            if (studentData.isPresent()) {
                CStudent updatedStudent = studentData.get();
                updatedStudent.setCode(cStudent.getCode());
                updatedStudent.setName(cStudent.getName());
                updatedStudent.setGender(cStudent.getGender());
                updatedStudent.setPhone(cStudent.getPhone());
                updatedStudent.setDateOfBirth(cStudent.getDateOfBirth());
                updatedStudent.setAddress(cStudent.getAddress());

                CStudent savedStudent = studentRepository.save(updatedStudent);
                return new ResponseEntity<>(savedStudent, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 3.1 READ - Get all students
    @CrossOrigin
    @GetMapping("/student/all")
    public List<CStudent> getAllStudents() {
        return studentRepository.findAll();
    }

    // 3.2 READ - Get students by class id
    @CrossOrigin
    @GetMapping("/class/{classId}/students")
    public List <CStudent> getStudentsByClass(@PathVariable(value = "classId") Long classId) {
        return studentRepository.findByLopHocId(classId);
    }

    // 3.3 READ - Get student details by id
    @CrossOrigin
    @GetMapping("/student/details/{id}")
    public CStudent getStudentById(@PathVariable Long id) {
        if (studentRepository.findById(id).isPresent())
            return studentRepository.findById(id).get();
        else
            return null;
    }

    // 4. DELETE - Delete student by id
    @CrossOrigin
    @DeleteMapping("/student/delete/{id}")
    public ResponseEntity<Object> deleteStudentById(@PathVariable Long id) {
        try {
            studentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
